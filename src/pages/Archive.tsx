import React, { useContext, useState } from "react";
import { ArchiveContext } from "./../context/ArchiveContext";

const Archive = () => {
  const { order } = useContext(ArchiveContext);

  return (
    <table className="archive__tabel">
      <thead className="table__head">
        <tr>
          <th className="side__col">Side</th>
          <th className="price__col">Price</th>
          <th className="instrument__col">Instrument</th>
          <th className="volume__col">Volume</th>
          <th className="timestamp__col">Timestamp</th>
        </tr>
      </thead>
      <tbody>
        {order.map((item) => {
          return (
            <tr key={item.timestamp} className="table__row">
              <td
                className="side__col"
                style={{
                  color: item.side === "Buy" ? "rgb(0, 184, 0)" : "red",
                }}
              >
                {item.side}
              </td>
              <td className="price__col">{item.price}</td>
              <td className="instrument__col">{item.instrument}</td>
              <td className="volume__col">{item.volume}</td>
              <td className="timestamp__col">{item.timestamp}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default Archive;
