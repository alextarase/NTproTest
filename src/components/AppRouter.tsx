import { Navigate, Route, Routes } from "react-router-dom";
import Archive from "../pages/Archive";
import Trading from "../pages/Trading";

const AppRouter = () => {
  return (
    <Routes>
      <Route path="/" element={<Trading />}></Route>
      <Route path="/archive" element={<Archive />}></Route>
      <Route path="*" element={<Navigate to="/" />} />
    </Routes>
  );
};

export default AppRouter;
