import React, { FC, useContext, useState } from "react";
import { ArchiveContext } from "../../context/ArchiveContext";
import { ICurrency } from "../../pages/Trading";
import currentDate from "../../utils/CurrentDate";
import cl from "./Order.module.css";

interface IOrder {
  setVisible: (_: boolean) => void;
  operationType: string;
  currency: ICurrency;
}

const Order: FC<IOrder> = ({ setVisible, operationType, currency }) => {
  const { setOrder } = useContext(ArchiveContext);
  const [value, setValue] = useState("");

  const handleClick = () => {
    setVisible(false);
  };
  const createOrder = () => {
    setOrder((prev: any) => [
      ...prev,
      {
        side: operationType,
        price: currency.price,
        instrument: currency.label,
        volume: Math.abs(parseInt(value)),
        timestamp: currentDate(),
      },
    ]);
    setVisible(false);
    setValue("");
  };
  const handleChangeVolume = (e: {
    target: { value: React.SetStateAction<string> };
  }) => {
    setValue(e.target.value);
  };
  return (
    <div>
      <div className={cl.header}>
        <h3 className={cl.header__title}>Make order</h3>
        <button onClick={handleClick} className={cl.header__button}>
          X
        </button>
      </div>
      <div>
        <div className={cl.body__info}>
          <p
            className={cl.info__type}
            style={{ color: operationType === "Buy" ? "green" : "red" }}
          >
            {operationType}
          </p>
          <p className={cl.info}>
            {currency.price} {currency.label}
          </p>
        </div>

        <div className={cl.input__volume}>
          <p>Volume</p>
          <input onChange={handleChangeVolume} value={value} type="number" />
        </div>
        <div className={cl.buttons}>
          <button className={cl.button__cansel} onClick={handleClick}>
            Cansel
          </button>
          <button onClick={createOrder} className={cl.button__ok}>
            Ok
          </button>
        </div>
      </div>
    </div>
  );
};

export default Order;
