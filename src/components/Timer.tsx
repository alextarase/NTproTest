import React, { useState } from "react";

const Timer = () => {
  const [time, setTime] = useState(new Date().toLocaleTimeString());

  setInterval(() => setTime(new Date().toLocaleTimeString()), 1000);
  return (
    <>
      <h2 className="timer">{time}</h2>
    </>
  );
};

export default Timer;
