import { createContext, SetStateAction } from "react";

export interface IOrder {
  side: string;
  price: string;
  instrument: string;
  volume: number;
  timestamp: string;
}

export const ArchiveContext = createContext({
  order: [
    {
      side: "",
      price: "",
      instrument: "",
      volume: 0,
      timestamp: "",
    },
  ],
  setOrder: (_value: any) => {},
});
