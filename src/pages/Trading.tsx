import React, { useEffect, useState } from "react";
import BuyButton from "../components/BuyButton";
import Modal from "../components/Modal/Modal";
import Order from "../components/Order/Order";
import SellButton from "../components/SellButton";
import Timer from "../components/Timer";

export interface ICurrency {
  price: string;
  label: string;
}

const Trading = () => {
  const [modal, setModal] = useState(false);
  const [operationType, setOperationType] = useState("buy");
  const initialState = {
    price: (Math.random() * 100).toFixed(4),
    label: "USD/RUB",
  };
  const [currency, setCurrency] = useState<ICurrency>(initialState);
  //Начальные значения
  const [currencies] = useState([
    { price: (Math.random() * 100).toFixed(4), label: "USD/RUB" },
    { price: (Math.random() * 100).toFixed(4), label: "EUR/USD" },
    { price: (Math.random() * 100).toFixed(4), label: "USD/JPY" },
  ]);

  const updatePrice = () => {
    setCurrency((prev) => ({
      price: (Math.random() * 100).toFixed(4).toString(),
      label: prev.label,
    }));
  };

  //Цена обновляется каждую секунду
  useEffect(() => {
    const interval = setInterval(updatePrice, 1000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  const handleCurrenceChange = (e: {
    target: { value: React.SetStateAction<string> };
  }) => {
    let lab = e.target.value.toString();
    let len = parseFloat(e.target.value).toString().length;
    let string = lab.slice(len + 1);
    setCurrency((prev) => ({
      ...prev,
      price: parseFloat(e.target.value).toString(),
      label: string,
    }));
  };

  const handleBuyClick = () => {
    setOperationType("Buy");
    setModal(true);
  };

  const handleSellClick = () => {
    setOperationType("Sell");
    setModal(true);
  };

  return (
    <div>
      <div className="container">
        <Modal visible={modal} setVisible={setModal}>
          <Order
            setVisible={setModal}
            operationType={operationType}
            currency={currency}
          />
        </Modal>
        <div>
          <Timer />
        </div>
        <select
          className="trading__select"
          defaultValue={currency.label}
          onChange={handleCurrenceChange}
        >
          {currencies.map((option) => {
            return (
              <option key={option.label} value={[option.price, option.label]}>
                {option.label}
              </option>
            );
          })}
        </select>
        <div className="trading__buttons">
          <BuyButton currency={currency} handleBuyClick={handleBuyClick} />
          <SellButton currency={currency} handleSellClick={handleSellClick} />
        </div>
      </div>
    </div>
  );
};

export default Trading;
