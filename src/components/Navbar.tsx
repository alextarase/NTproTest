import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="navbar">
      <NavLink className={"navbar__item"} to={"/"}>
        Trading
      </NavLink>
      <NavLink className={"navbar__item"} to={"/archive"}>
        Archive
      </NavLink>
    </div>
  );
};

export default Navbar;
