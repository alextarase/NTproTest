import { useState } from "react";
import "./App.css";
import Navbar from "./components/Navbar";
import AppRouter from "./components/AppRouter";
import { ArchiveContext } from "./context/ArchiveContext";
import currentDate from "./utils/CurrentDate";

function App() {
  const [order, setOrder] = useState([
    {
      side: "Sell",
      price: "65.14",
      instrument: "EUR/USD",
      volume: 2000000000,
      timestamp: "2022.02.01 13:55:18.578",
    },
    {
      side: "Buy",
      price: "65.14",
      instrument: "EUR/USD",
      volume: 15015000,
      timestamp: "2022.02.01 13:56:18.578",
    },
  ]);

  return (
    <ArchiveContext.Provider value={{ order, setOrder }}>
      <div className="App">
        <Navbar />
        <AppRouter />
      </div>
    </ArchiveContext.Provider>
  );
}

export default App;
