import React, { FC } from "react";
import { ICurrency } from "../pages/Trading";

interface IBuyButton {
  currency: ICurrency;
  handleBuyClick: () => void;
}

const BuyButton: FC<IBuyButton> = ({ currency, handleBuyClick }) => {
  return (
    <div className="block buy">
      <button className="button buy">BUY</button>
      <p onClick={handleBuyClick}>{(+currency.price + 0.1).toFixed(4)}</p>
    </div>
  );
};

export default BuyButton;
