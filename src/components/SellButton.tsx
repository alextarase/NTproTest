import React, { FC } from "react";
import { ICurrency } from "../pages/Trading";

interface ISellButton {
  currency: ICurrency;
  handleSellClick: () => void;
}

const SallButton: FC<ISellButton> = ({ currency, handleSellClick }) => {
  return (
    <div className="block sell">
      <button className="button sell">SELL</button>
      <p onClick={handleSellClick}> {(+currency.price - 0.1).toFixed(4)}</p>
    </div>
  );
};

export default SallButton;
